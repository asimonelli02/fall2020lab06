
//Antonio Simonelli
import java.util.Random;

public class RpsGame {
	private int wins = 0;
	private int ties = 0;
	private int losses = 0;

	Random r = new Random();

	public int getWins() {
		return this.wins;
	}

	public int getTies() {
		return this.ties;
	}

	public int getLosses() {
		return this.losses;
	}

	public String playRound(int player) {
		int computer = r.nextInt(3);
		String result = "";
		switch (computer) {
		case 0:
			if (player == 0) {
				this.ties++;
				result = "Computer plays rock and tied";
			}
			if (player == 1) {
				this.wins++;
				result = "Computer plays rock and you won";
			}
			if (player == 2) {
				this.losses++;
				result = "Computer plays rock and the computer won";
			}
		case 1:
			if (player == 0) {
				this.losses++;
				result = "Computer plays paper and the computer won";
			}
			if (player == 1) {
				this.ties++;
				result = "Computer plays paper and tied";
			}
			if (player == 0) {
				this.wins++;
				result = "Computer plays paper and you won";
			}
		case 2:
			if (player == 0) {
				this.wins++;
				result = "Computer plays scissor and you won";
			}
			if (player == 1) {
				this.losses++;
				result = "Computer plays scissor and the computer won";
			}
			if (player == 2) {
				this.ties++;
				result = "Computer plays scissor and tied";
			}
		}
		return result;

	}
}
