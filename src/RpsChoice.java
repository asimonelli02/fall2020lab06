
//Antonio Simonelli
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private int player;
	private RpsGame game;
	
	public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, int player, RpsGame game) {
		this.message = message;
		this.wins =wins;
		this.losses = losses;
		this.ties = ties;
		this.player = player;
		this.game = game;
	}

	@Override
	public void handle(ActionEvent e) {
		String gameResult = game.playRound(player);
		message.setText(gameResult);
		wins.setText("wins: " + game.getWins());
		losses.setText("losses: " + game.getLosses());
		ties.setText("ties: " + game.getTies());
	}

}
