
//Antonio Simonelli
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
	private RpsGame game = new RpsGame();

	public void start(Stage stage) {
		Group root = new Group();

		HBox buttons = new HBox();
		HBox textField = new HBox();
		VBox overall = new VBox();
		Button rock = new Button("Rock");
		Button paper = new Button("Paper");
		Button scissor = new Button("Scissor");
		TextField welcome = new TextField("Welcome");
		welcome.setPrefWidth(200);
		TextField wins = new TextField("Wins: ");
		TextField losses = new TextField("Losses: ");
		TextField ties = new TextField("Ties: ");
		buttons.getChildren().addAll(rock, paper, scissor);
		textField.getChildren().addAll(welcome, wins, losses, ties);
		overall.getChildren().addAll(buttons, textField);
		root.getChildren().add(overall);
		
		RpsChoice rockChoice = new RpsChoice(welcome, wins, losses, ties, 0, game);
		RpsChoice paperChoice = new RpsChoice(welcome, wins, losses, ties, 1, game);
		RpsChoice scissorChoice = new RpsChoice(welcome, wins, losses, ties, 2, game);
		
		rock.setOnAction(rockChoice);
		paper.setOnAction(paperChoice);
		scissor.setOnAction(scissorChoice);

		// scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);

		// associate scene to stage and show
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);

		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
